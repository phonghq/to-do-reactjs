import React from 'react'
import PropTypes from 'prop-types'

import ToDoItems from './TodoItems'

const TodoList = ({ tasks }) => {
  return (
      <ul className="theList">
        {
            
          tasks.map(({key, text}) =>
              <ToDoItems key={key} text={text}/>
          )
        }
      </ul>
  )
}

TodoList.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
        key: PropTypes.number.isRequired,
        text: PropTypes.string.isRequired
    })
  ).isRequired
}

export default TodoList