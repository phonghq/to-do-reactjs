import React from 'react'
import PropTypes from 'prop-types'

const ToDoItems = ({ text }) => {
    console.log(text);
    return (
        <li>
            {text}
        </li>
    )
}

ToDoItems.propTypes = {
    text: PropTypes.string.isRequired
}

export default ToDoItems