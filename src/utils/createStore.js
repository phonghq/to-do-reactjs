const ActionTypes = {
    INIT: '@@redux/INIT'
}
const createStore = (reducer, initialState) =>{
    const store = {};
    store.state = initialState;
    store.listener = [];
    store.getState = () => store.state;
    store.subscribe = (listener) => {
        store.listener.push(listener);
    }
    store.dispatch = (action) => {
        store.state = reducer(store.state, action)
        store.listener.forEach(listener => listener())
    }
    store.dispatch({
        type: ActionTypes.INIT
    })
    return store;
}
export default createStore;